## full_oppo6769-user 10 QP1A.190711.020 68b77aba7cb33275 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: aosp_RMX2020-userdebug
- Release Version: 12
- Id: SD1A.210817.036
- Incremental: 1637498178
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP1A.211105.002/7743617:user/release-keys
- OTA version: 
- Branch: full_oppo6769-user-10-QP1A.190711.020-68b77aba7cb33275-release-keys
- Repo: realme_rmx2020_dump_22409


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
